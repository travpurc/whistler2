from flask import request, url_for, Blueprint, render_template
from flask.ext.api import FlaskAPI, status, exceptions
import sys
sys.path.insert(0, 'Whister2Server/Ocean')
from OceanClient import OceanClient

app = FlaskAPI(__name__.split('.')[0])
views = Blueprint('views', __name__, url_prefix='/pages')
oceanClient = OceanClient()

notes = {
    0: 'Whistler2, now with Python!',
    1: 'I am using Python for this simple API example, it returns JSON',
    2: 'I ran out of things to say... I should stop making these',
}

def note_repr(key):
    return {
        'url': request.host_url.rstrip('/') + url_for('notes_detail', key=key),
        'text': oceanClient.GetPersonData() #notes[key]
    }


@app.route("/", methods=['GET', 'POST'])
def notes_list():
    """
    List or create notes.
    """
    
    if request.method == 'POST':
        note = str(request.data.get('text', ''))
        idx = max(notes.keys()) + 1
        notes[idx] = note
        return note_repr(idx), status.HTTP_201_CREATED

    # request.method == 'GET'
    return [note_repr(idx) for idx in sorted(notes.keys())]


@app.route("/<int:key>/", methods=['GET', 'PUT', 'DELETE'])
def notes_detail(key):
    """
    Retrieve, update or delete note instances.
    """
    if request.method == 'PUT':
        note = str(request.data.get('text', ''))
        notes[key] = note
        return note_repr(key)

    elif request.method == 'DELETE':
        notes.pop(key, None)
        return '', status.HTTP_204_NO_CONTENT

    # request.method == 'GET'
    if key not in notes:
        raise exceptions.NotFound()
    return note_repr(key)


if __name__ == "__main__":
    app.run(debug=True)