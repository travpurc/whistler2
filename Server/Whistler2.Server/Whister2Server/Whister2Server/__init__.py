"""
The flask application package.
"""

from flask import Flask
app = Flask(__name__)

import requests

import sys
sys.path.insert(0, '/Whister2Server/Ocean')

import Whister2Server.views
import OceanClient