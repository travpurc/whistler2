"""
This script runs the Whister2Server application using a development server.

TODO: This can be the main application once we can get blueprints working correctly. 
I am unaware why it is being annoying at the moment
"""

from os import environ
from Whister2Server import app
from Whister2Server.views import views
from flask import Flask

app = Flask(__name__.split('.')[0])
app.register_blueprint(views, url_prefix='/views')

if __name__ == '__main__':
    #HOST = environ.get('SERVER_HOST', 'localhost')
    #try:
    #    PORT = int(environ.get('SERVER_PORT', '5555'))
    #except ValueError:
    #    PORT = 5555
    #app.run(HOST, PORT, debug=True)
    app.run(debug=True)

